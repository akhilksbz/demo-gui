#ifndef RUNDIR_H
#define RUNDIR_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QFileSystemModel>

namespace Ui {
    class runDir;
}

class runDir : public QDialog
{
    Q_OBJECT

public:
    explicit runDir(QWidget *parent = nullptr);
    QFileSystemModel *model = new QFileSystemModel(this);
    QString savdir();
    ~runDir();


private slots:
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
    void on_treeView_clicked(const QModelIndex &index);
    void on_pushButton_3_clicked();

    void on_lineEdit_returnPressed();

private:
    Ui::runDir *ui;
};

#endif // RUNDIR_H
