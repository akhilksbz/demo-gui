#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <about.h>
#include <howto.h>
#include <QStandardItemModel>
#include <rundir.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    MainWindow::setWindowTitle("Demo Project");
    PCS = new QProcess(this);
    ui->radioButton->setChecked(true);
    ui->progressBar->setValue(0);
    ui->pushButton_3->setEnabled(false);
    connect(PCS,SIGNAL(readyReadStandardOutput()),this,SLOT(strtslot()));
    connect(PCS,SIGNAL(finished(int,QProcess::ExitStatus)),this,SLOT(endslot()));
    Cbox_filter();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionAbout_triggered()
{
    About abt;
    abt.setWindowTitle("About Demo Project");
    abt.setModal(true);
    abt.exec();
}

void MainWindow::on_actionHow_to_use_triggered()
{
    Howto abt;
    abt.setWindowTitle("Instructions");
    abt.setModal(true);
    abt.exec();
}

void MainWindow::on_radioButton_2_clicked()
{
    test11.Visibility_value = 1;
    Cbox_filter();
}

void MainWindow::on_radioButton_clicked()
{
    test11.Visibility_value = 0;
    Cbox_filter();
}

void MainWindow::on_radioButton_3_clicked()
{
    test11.Visibility_value = 2;
    Cbox_filter();
}

void MainWindow::Cbox_filter()
{
    if(ui->radioButton->isChecked())
    {
        ui->comboBox_2->setCurrentIndex(0);
        ui->comboBox_2->setItemData(0,33,Qt::UserRole-1);
        ui->comboBox_2->setItemData(1,33,Qt::UserRole-1);
        ui->comboBox_2->setItemData(2,33,Qt::UserRole-1);
        ui->comboBox_2->setItemData(3,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(4,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(5,0,Qt::UserRole-1);
    }else if(ui->radioButton_2->isChecked())
    {
        ui->comboBox_2->setCurrentIndex(3);
        ui->comboBox_2->setItemData(0,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(1,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(2,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(3,33,Qt::UserRole-1);
        ui->comboBox_2->setItemData(4,33,Qt::UserRole-1);
        ui->comboBox_2->setItemData(5,0,Qt::UserRole-1);
    }else
    {
        ui->comboBox_2->setCurrentIndex(5);
        ui->comboBox_2->setItemData(0,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(1,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(2,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(3,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(4,0,Qt::UserRole-1);
        ui->comboBox_2->setItemData(5,33,Qt::UserRole-1);
    }
    test11.Method_used = ui->comboBox_2->currentIndex();
    inspectionData();
}

void MainWindow::on_pushButton_clicked()
{
    runDir abt;
    abt.setWindowTitle("Choose Working Directory");
    abt.setModal(true);
    if(abt.exec() == QDialog::Accepted)
    {
      ui->lineEdit->setText(abt.savdir());
      test11.Input_path = abt.savdir();
    }
    inspectionData();
}

void MainWindow::on_pushButton_2_clicked()
{
    runDir abt;
    abt.setWindowTitle("Choose Saving Directory");
    abt.setModal(true);
    if(abt.exec() == QDialog::Accepted)
    {
      ui->lineEdit_2->setText(abt.savdir());
      test11.Save_path = abt.savdir();
    }
    inspectionData();
}


void MainWindow::inspectionData()
{
    ui->textBrowser->clear();
    ui->progressBar->setValue(0);
    QDir dir(test11.Input_path);
    if(dir.exists())
    {
        ui->textBrowser->setText("InputPath : " + test11.Input_path);
        test11.complt = true;
    }
    else
    {
        ui->textBrowser->setText("InputPath is Not Valid");
        test11.complt = false;
    }
    dir.setPath(test11.Save_path);
    if(dir.exists())
    {
        ui->textBrowser->append("SavePath : " + test11.Save_path);
        test11.complt = true;
    }
    else
    {
        ui->textBrowser->append("SavePath is Not Valid");
        test11.complt = false;
    }
    ui->textBrowser->append("Visibility Selected: " + QString::number(test11.Visibility_value));
    if(test11.IP_cam)
    {
        ui->textBrowser->append("Ip Camera used");
    }else
    {
        ui->textBrowser->append("Ip Camera not used");
    }
    ui->textBrowser->append("Currently Selected Method" + QString::number(test11.Method_used + 1));
    if(test11.complt)
    {
        ui->textBrowser->append("Ready");
        ui->pushButton_3->setEnabled(true);
    }
    else
    {
        ui->textBrowser->append("Not Ready");
        ui->pushButton_3->setEnabled(false);
    }
}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    test11.IP_cam = ui->checkBox->checkState();
    inspectionData();
}

void MainWindow::on_comboBox_2_activated(int index)
{
    test11.Method_used = ui->comboBox_2->currentIndex();
    inspectionData();
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionInput_File_Path_triggered()
{
    on_pushButton_clicked();
}

void MainWindow::on_actionSave_File_Path_triggered()
{
    on_pushButton_2_clicked();
}

void MainWindow::on_lineEdit_returnPressed()
{
    test11.Input_path = ui->lineEdit->text();
    inspectionData();
}

void MainWindow::on_lineEdit_2_returnPressed()
{
    test11.Save_path = ui->lineEdit_2->text();
    inspectionData();
}

void MainWindow::on_actionExecute_triggered()
{
    //Execute Scipt
    if(!test11.complt) return;
    on_pushButton_3_clicked();
}

void MainWindow::on_actionTerminate_triggered()
{
    //Terminate Script here
    QMessageBox::StandardButton reply = QMessageBox::question(this,"Confirm","Do you want to Exit the Program",QMessageBox::Yes | QMessageBox::No);
    if(reply != QMessageBox::Yes) return;
    close();
}

void MainWindow::on_pushButton_4_clicked()
{
    on_actionTerminate_triggered();
}

void MainWindow::on_pushButton_3_clicked()
{
    inspectionData();
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Confirm","Do you want to Execute With current Configuration",QMessageBox::Yes | QMessageBox::No);
    if(reply != QMessageBox::Yes) return;
    //Execute Scipt
    PCS->setWorkingDirectory(QCoreApplication::applicationDirPath());
    PCS->start("python",param_Parser());
    PCS->setReadChannel(QProcess::StandardOutput);
    ui->textBrowser->append("started py Script execution");
    ui->progressBar->setValue(10);
    ui->pushButton_3->setEnabled(false);
}


QStringList MainWindow::param_Parser()
{
    QStringList params;
    if(test11.IP_cam)
    {
        params <<"test.py"<<test11.Input_path<<test11.Save_path<<QString::number(test11.Visibility_value)<<"True"<<"method"+QString::number(test11.Method_used+1);
    }else{
        params <<"test.py"<<test11.Input_path<<test11.Save_path<<QString::number(test11.Visibility_value)<<"False"<<"method"+QString::number(test11.Method_used+1);
    }
    return params;
}

void MainWindow::strtslot()
{
    ui->textBrowser->append(PCS->readAllStandardOutput());
    ui->progressBar->setValue(50);
}

void MainWindow::endslot()
{
    ui->textBrowser->append("completed py Script execution");
    ui->progressBar->setValue(100);
    PCS->waitForFinished();
    PCS->close();
}
