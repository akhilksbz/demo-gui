#include "rundir.h"
#include "ui_rundir.h"
#include <QFileSystemModel>

runDir::runDir(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::runDir)
{
    ui->setupUi(this);
    ui->treeView->setModel(model);
    QModelIndex index = model->setRootPath(QDir::rootPath());
    QString path = model->fileInfo(index).absoluteFilePath();
    ui->lineEdit->setText(path);
    ui->treeView->setCurrentIndex(index);
    ui->treeView->scrollTo(index);
    ui->treeView->resizeColumnToContents(0);
    //
}

runDir::~runDir()
{
    delete ui;
}

void runDir::on_pushButton_clicked()
{
   QModelIndex index = ui->treeView->currentIndex();
   if(!index.isValid()) return;
   QString name = QInputDialog::getText(this,"New Folder","Enter Folder Name");
   if(name.isEmpty()) return;
   model->mkdir(index,name);
   index = model->setRootPath(model->fileInfo(index).absoluteFilePath()+"/"+ name);
   ui->treeView->setCurrentIndex(index);
   ui->treeView->scrollTo(index);
   ui->treeView->resizeColumnToContents(0);
   on_treeView_clicked(index);
}


void runDir::on_pushButton_2_clicked()
{
    QModelIndex index = ui->treeView->currentIndex();
    if(!index.isValid()) return;
    if(model->fileInfo(index).isDir())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this,"Delete","Do you want to remove folder",QMessageBox::Yes | QMessageBox::No);
        if(reply == QMessageBox::Yes)
        {
           model->rmdir(index);
        }
    }
    else{
       QMessageBox::information(this,"Error","Can't Remove");
    }
    index = ui->treeView->indexAbove(index);
    QString path = model->fileInfo(index).absoluteFilePath();
    ui->treeView->indexBelow(index);
    on_treeView_clicked(index.parent());
}

void runDir::on_treeView_clicked(const QModelIndex &index)
{
    QString path = model->fileInfo(index).absoluteFilePath();
    ui->lineEdit->setText(path);
}

void runDir::on_pushButton_3_clicked()
{
    savdir();
    accept();
}

QString runDir::savdir()
{
    return ui->lineEdit->text();
}

void runDir::on_lineEdit_returnPressed()
{
    QModelIndex index = model->setRootPath(ui->lineEdit->text());
    ui->treeView->setCurrentIndex(index);
    ui->treeView->scrollTo(index);
    ui->treeView->resizeColumnToContents(0);
    // input
}
