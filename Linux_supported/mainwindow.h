#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtWidgets>
#include <QtGui>
#include <QThread>
#include <QTextStream>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    struct inspctAll{
        QString Input_path = "/no";
        QString Save_path = "/no";
        bool IP_cam = false;
        int Visibility_value = 0;
        int Method_used = 0;
        bool complt = false;
    }test11;




private slots:
    void on_actionAbout_triggered();

    void on_actionHow_to_use_triggered();

    void on_radioButton_2_clicked();

    void on_radioButton_clicked();

    void on_radioButton_3_clicked();

    void Cbox_filter();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void inspectionData();

    void on_checkBox_stateChanged(int arg1);

    void on_comboBox_2_activated(int index);

    void on_actionExit_triggered();

    void on_actionInput_File_Path_triggered();

    void on_actionSave_File_Path_triggered();

    void on_lineEdit_returnPressed();

    void on_lineEdit_2_returnPressed();

    void on_actionExecute_triggered();

    void on_actionTerminate_triggered();

    QStringList param_Parser();

    void on_pushButton_4_clicked();

    void strtslot();

    void endslot();

private:
    Ui::MainWindow *ui;
    QProcess *PCS;

};
#endif // MAINWINDOW_H
