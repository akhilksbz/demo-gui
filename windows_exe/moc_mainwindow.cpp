/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../ProjectV1/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[27];
    char stringdata0[554];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 24), // "on_actionAbout_triggered"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 29), // "on_actionHow_to_use_triggered"
QT_MOC_LITERAL(4, 67, 24), // "on_radioButton_2_clicked"
QT_MOC_LITERAL(5, 92, 22), // "on_radioButton_clicked"
QT_MOC_LITERAL(6, 115, 24), // "on_radioButton_3_clicked"
QT_MOC_LITERAL(7, 140, 11), // "Cbox_filter"
QT_MOC_LITERAL(8, 152, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(9, 174, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(10, 198, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(11, 222, 14), // "inspectionData"
QT_MOC_LITERAL(12, 237, 24), // "on_checkBox_stateChanged"
QT_MOC_LITERAL(13, 262, 4), // "arg1"
QT_MOC_LITERAL(14, 267, 23), // "on_comboBox_2_activated"
QT_MOC_LITERAL(15, 291, 5), // "index"
QT_MOC_LITERAL(16, 297, 23), // "on_actionExit_triggered"
QT_MOC_LITERAL(17, 321, 34), // "on_actionInput_File_Path_trig..."
QT_MOC_LITERAL(18, 356, 33), // "on_actionSave_File_Path_trigg..."
QT_MOC_LITERAL(19, 390, 25), // "on_lineEdit_returnPressed"
QT_MOC_LITERAL(20, 416, 27), // "on_lineEdit_2_returnPressed"
QT_MOC_LITERAL(21, 444, 26), // "on_actionExecute_triggered"
QT_MOC_LITERAL(22, 471, 28), // "on_actionTerminate_triggered"
QT_MOC_LITERAL(23, 500, 12), // "param_Parser"
QT_MOC_LITERAL(24, 513, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(25, 537, 8), // "strtslot"
QT_MOC_LITERAL(26, 546, 7) // "endslot"

    },
    "MainWindow\0on_actionAbout_triggered\0"
    "\0on_actionHow_to_use_triggered\0"
    "on_radioButton_2_clicked\0"
    "on_radioButton_clicked\0on_radioButton_3_clicked\0"
    "Cbox_filter\0on_pushButton_clicked\0"
    "on_pushButton_2_clicked\0on_pushButton_3_clicked\0"
    "inspectionData\0on_checkBox_stateChanged\0"
    "arg1\0on_comboBox_2_activated\0index\0"
    "on_actionExit_triggered\0"
    "on_actionInput_File_Path_triggered\0"
    "on_actionSave_File_Path_triggered\0"
    "on_lineEdit_returnPressed\0"
    "on_lineEdit_2_returnPressed\0"
    "on_actionExecute_triggered\0"
    "on_actionTerminate_triggered\0param_Parser\0"
    "on_pushButton_4_clicked\0strtslot\0"
    "endslot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  129,    2, 0x08 /* Private */,
       3,    0,  130,    2, 0x08 /* Private */,
       4,    0,  131,    2, 0x08 /* Private */,
       5,    0,  132,    2, 0x08 /* Private */,
       6,    0,  133,    2, 0x08 /* Private */,
       7,    0,  134,    2, 0x08 /* Private */,
       8,    0,  135,    2, 0x08 /* Private */,
       9,    0,  136,    2, 0x08 /* Private */,
      10,    0,  137,    2, 0x08 /* Private */,
      11,    0,  138,    2, 0x08 /* Private */,
      12,    1,  139,    2, 0x08 /* Private */,
      14,    1,  142,    2, 0x08 /* Private */,
      16,    0,  145,    2, 0x08 /* Private */,
      17,    0,  146,    2, 0x08 /* Private */,
      18,    0,  147,    2, 0x08 /* Private */,
      19,    0,  148,    2, 0x08 /* Private */,
      20,    0,  149,    2, 0x08 /* Private */,
      21,    0,  150,    2, 0x08 /* Private */,
      22,    0,  151,    2, 0x08 /* Private */,
      23,    0,  152,    2, 0x08 /* Private */,
      24,    0,  153,    2, 0x08 /* Private */,
      25,    0,  154,    2, 0x08 /* Private */,
      26,    0,  155,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::QStringList,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionAbout_triggered(); break;
        case 1: _t->on_actionHow_to_use_triggered(); break;
        case 2: _t->on_radioButton_2_clicked(); break;
        case 3: _t->on_radioButton_clicked(); break;
        case 4: _t->on_radioButton_3_clicked(); break;
        case 5: _t->Cbox_filter(); break;
        case 6: _t->on_pushButton_clicked(); break;
        case 7: _t->on_pushButton_2_clicked(); break;
        case 8: _t->on_pushButton_3_clicked(); break;
        case 9: _t->inspectionData(); break;
        case 10: _t->on_checkBox_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_comboBox_2_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_actionExit_triggered(); break;
        case 13: _t->on_actionInput_File_Path_triggered(); break;
        case 14: _t->on_actionSave_File_Path_triggered(); break;
        case 15: _t->on_lineEdit_returnPressed(); break;
        case 16: _t->on_lineEdit_2_returnPressed(); break;
        case 17: _t->on_actionExecute_triggered(); break;
        case 18: _t->on_actionTerminate_triggered(); break;
        case 19: { QStringList _r = _t->param_Parser();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = std::move(_r); }  break;
        case 20: _t->on_pushButton_4_clicked(); break;
        case 21: _t->strtslot(); break;
        case 22: _t->endslot(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
